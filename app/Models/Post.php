<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $table = 'tp_post';
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    protected $fillable = [
        'id',
        'name',
        'content',
        'status',
    ];

    public function error()
    {
        return $this->belongsToMany('App\Models\Error', 'tp_error_post_rel', 'post_id', 'error_id');
    }

    public function getPostRel()
    {
        return $this->with('error');
    }
}
