<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'tp_link';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'link',
        'author',
        'error',
    ];
}
