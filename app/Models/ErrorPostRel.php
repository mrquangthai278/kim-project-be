<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ErrorPostRel extends Model
{
    protected $table = 'tp_error_post_rel';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'error_id',
        'post_id'
    ];
}
