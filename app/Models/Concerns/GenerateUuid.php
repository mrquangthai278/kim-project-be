<?php

namespace App\Models\Concerns;
use Webpatser\Uuid\Uuid;
trait GenerateUuid
{
    protected static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string)Uuid::generate(4);
        });
    }
}
