<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\ErrorRepositoryInterface;

class ErrorController extends Controller
{
    private $error;

    public function __construct(ErrorRepositoryInterface $errorRepositoryInterface)
    {
        $this->error = $errorRepositoryInterface;
    }

    public function getListError(Request $request)
    {
        try {
            $data = $this->error->getErrorLib($request->all());
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function all(Request $request)
    {
        try {
            $data = $this->error->all();
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function create(Request $request)
    {
        try {
            $dataCreate = $request->all();
            if (!$request->word) {

                $whatIWantWord = substr($request->description, 0, strpos($request->description,  "—"));
                $whatIWantDecs = substr($request->description, strpos($request->description, "—"));

                $dataCreate['word'] = $whatIWantWord;

                $dataCreate['description'] = $whatIWantDecs;
            }
            if (!$dataCreate['word'] || !$dataCreate['description'])  return $this->apiError('Wrong Format', 400);
            $data = $this->error->save($dataCreate);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function createNote(Request $request)
    {
        try {
            $dataCreate = $request->all();
            $data = $this->error->saveNote($dataCreate);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = $this->error->delete($request->id);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $dataEdit['word'] = $request->word;
            $dataEdit['description'] = $request->description;
            $dataEdit['priority'] = $request->priority;
            $data = $this->error->update($dataEdit, $request->id);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }
}
