<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\LinkRepositoryInterface;

class LinkController extends Controller
{
    private $link;

    public function __construct(LinkRepositoryInterface $linkRepositoryInterface)
    {
        $this->link = $linkRepositoryInterface;
    }

    public function create(Request $request)
    {
        try {
            $dataCreate = $request->all();
            if ($dataCreate['error']) $dataCreate['error'] = json_encode($dataCreate['error']);
            $data = $this->link->save($dataCreate);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function getLink(Request $request)
    {
        try {
            $link = $request->link;
            $data = $this->link->getLink($link);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $dataEdit = $request->all();
            if ($dataEdit['error']) $dataEdit['error'] = json_encode($dataEdit['error']);
            $data = $this->link->update($dataEdit, $dataEdit['id']);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function export(Request $request)
    {
        try {
            // Data Export
            $dataAuthor = $request->author ? $request->author : '';
            $dataPost = $request->post ? $request->post : '';
            $dataError = $request->error ? $request->error : [];
            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $section = $phpWord->addSection();

            // Title
            $fontStyleName = 'oneUserDefinedStyle';

            $phpWord->addFontStyle(
                $fontStyleName,
                array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
            );
            $section->addText(
                'Author: ' . $dataAuthor,
                $fontStyleName
            );

            $section->addText(
                'Post: ' . $dataPost,
                $fontStyleName
            );

            $section->addText(
                'Error: ',
                $fontStyleName
            );

            // List Error   
            foreach ($dataError as $key => $value) {
                $section->addText(
                    $value['time'] . ' - ' . $value['error'],
                    array('name' => 'Tahoma', 'size' => 10)
                );
            }

            $fontStyle = new \PhpOffice\PhpWord\Style\Font();
            $fontStyle->setBold(true);
            $fontStyle->setName('Tahoma');
            $fontStyle->setSize(13);
            $myTextElement = $section->addText('Have a good time!');
            $myTextElement->setFontStyle($fontStyle);

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

            $objWriter->save('../storage/files/'.$dataAuthor.$dataPost.time().'.docx');
            return $this->apiSuccess($objWriter);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }
}
