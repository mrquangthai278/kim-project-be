<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Validator;
use JWTAuth;
use JWTAuthException;

class UserController extends Controller
{
    private $user;
    public function __construct(UserRepositoryInterface $userRepositoryInterface)
    {
        $this->user = $userRepositoryInterface;
    }

    public function getUserByEmail(Request $request)
    {
        try {
            $user = $this->user->getUserByEmail($request->email);

            if ($user) {
                return $this->apiSuccess($user);
            } else {
                return $this->apiError();
            }
        } catch (Exception $e) {

            return $this->apiError($e);
        }
    }

    public function register(Request $request)
    {
        try {
            $data = $request->all();

            $rules = [
                'email' => 'required|email|unique:tp_users,email',
                'password' => 'required|confirmed|min:6|max:32',
                'password_confirmation' => 'required|min:6|max:32'
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                $this->apiError(['errors' => $validator->errors()]);
            }

            $data['email'] = $request->email;
            $data['password'] = Hash::make($request->password);

            $saveUser = $this->user->save($data);

            if ($saveUser) {

                return $this->apiSuccess(['email' => $saveUser->email]);
            }
        } catch (Exception $e) {

            return $this->apiError($e);
        }
    }

    public function login(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');

            $token = null;

            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->apiError('Invalid email or password', 401);
            }
            return $this->apiSuccess($token, 200);
        } catch (Exception $e) {

            return $this->apiError($e);
        }
    }

    public function getUserInfo(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->header('Authorization'));

            return response()->json($user);
        } catch (Exception $e) {

            return $this->apiError($e);
        }
    }
}
