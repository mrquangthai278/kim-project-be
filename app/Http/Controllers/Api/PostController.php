<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\PostRepositoryInterface;

class PostController extends Controller
{
    private $post;

    public function __construct(PostRepositoryInterface $postRepositoryInterface)
    {
        $this->post = $postRepositoryInterface;
    }

    public function getListPost(Request $request)
    {
        try {
            $data = $this->post->getPostList($request->all());
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function all(Request $request)
    {
        try {
            $data = $this->post->all();
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function create(Request $request)
    {
        try {
            $dataCreate = $request->all();
            $data = $this->post->save($dataCreate);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = $this->post->delete($request->id);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $dataEdit['name'] = $request->name;
            $dataEdit['content'] = $request->content;
            $dataEdit['status'] = $request->status;
            $data = $this->post->update($dataEdit, $request->id);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }
    public function getError(Request $request, $post_id)
    {
        try {
            $data = $this->post->getError($post_id);
            return $this->apiSuccess($data);
        } catch (Exception $e) {
            return $this->apiError($e);
        }
    }
}
