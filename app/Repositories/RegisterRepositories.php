<?php 
namespace App\Repositories; 
use Illuminate\Support\ServiceProvider; 
class RegisterRepositories extends ServiceProvider 
{ 
  public function register() 
  { 
 
        $this->app->bind( 
           'App\Repositories\Interfaces\ErrorRepositoryInterface', 
           'App\Repositories\Repository\ErrorRepository' 
 
        ); 
        $this->app->bind( 
           'App\Repositories\Interfaces\LinkRepositoryInterface', 
           'App\Repositories\Repository\LinkRepository' 
 
        ); 
        $this->app->bind( 
           'App\Repositories\Interfaces\PostRepositoryInterface', 
           'App\Repositories\Repository\PostRepository' 
 
        ); 
        $this->app->bind( 
           'App\Repositories\Interfaces\UserRepositoryInterface', 
           'App\Repositories\Repository\UserRepository' 
 
        ); 
  } 
} 
