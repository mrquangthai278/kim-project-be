<?php

namespace App\Repositories\Repository;

use App\Repositories\Interfaces\LinkRepositoryInterface;
use App\Models\Link;

class LinkRepository implements LinkRepositoryInterface
{
    private $link;
    public function __construct()
    {
        $this->link = new Link();
    }


    public function get($id, $columns = array('*'))
    {
        $data = $this->link->find($id, $columns);
        if ($data) {
            return $data;
        }
        return null;
    }
    public function all($columns = array('*'))
    {
        $listData = $this->link->get($columns);
        return $listData;
    }
    public function paginate($perPage = 15, $columns = array('*'))
    {
        $listData = $this->link->paginate($perPage, $columns);
        return $listData;
    }
    public function save(array $data)
    {
        $currentLink = $this->link->create($data);
        if ($currentLink->error)  $currentLink->error =  json_decode($currentLink->error);
        return $currentLink;
    }
    public function update(array $data, $id)
    {
        $dep =  $this->link->find($id);
        if ($dep) {
            foreach ($dep->getFillable() as $field) {
                if (array_key_exists($field, $data)) {
                    $dep->$field = $data[$field];
                }
            }
            $saveDep = $dep->save();
            if ($saveDep) {
                $currentLink = $this->link->find($id);

                if ($currentLink->error) $currentLink->error = json_decode($currentLink->error);
                return $currentLink;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->link->where($column, $value)->first();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->link;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->first();


        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->link->where($column, $value)->get();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->link;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

        if ($data) {
            return $data;
        }
        return null;
    }
    public function delete($id)
    {
        $del = $this->link->find($id);
        if ($del !== null) {
            $del->delete();
            return true;
        } else {
            return false;
        }
    }

    public function deleteMulti(array $data)
    {
        $del = $this->link->whereIn("id", $data["list_id"])->delete();
        if ($del) {

            return true;
        } else {
            return false;
        }
    }

    public function getLink($link = '')
    {
        $linkData = $this->link->where('link', 'like', $link)->first();
        if (!$linkData) return null;
        if ($linkData->error) $linkData->error = json_decode($linkData->error);
        return $linkData;
    }
}
