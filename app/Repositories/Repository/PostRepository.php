<?php

namespace App\Repositories\Repository;

use Illuminate\Support\Collection;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Models\Post;
use App\Models\Error;

class PostRepository implements PostRepositoryInterface
{
    private $post;
    public function __construct()
    {
        $this->post = new Post();
        $this->error = new Error();
    }


    public function get($id, $columns = array('*'))
    {
        $data = $this->post->find($id, $columns);
        if ($data) {
            return $data;
        }
        return null;
    }
    public function all($columns = array('*'))
    {
        $listData = $this->post->get($columns);
        return $listData;
    }
    public function paginate($perPage = 15, $columns = array('*'))
    {
        $listData = $this->post->paginate($perPage, $columns);
        return $listData;
    }
    public function save(array $data)
    {
        return $this->post->create($data);
    }
    public function update(array $data, $id)
    {
        $dep =  $this->post->find($id);
        if ($dep) {
            foreach ($dep->getFillable() as $field) {
                if (array_key_exists($field, $data)) {
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->post->where($column, $value)->first();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->post;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->first();


        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->post->where($column, $value)->get();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->post;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

        if ($data) {
            return $data;
        }
        return null;
    }
    public function delete($id)
    {
        $del = $this->post->find($id);
        if ($del !== null) {
            $del->delete();
            return true;
        } else {
            return false;
        }
    }

    public function deleteMulti(array $data)
    {
        $del = $this->post->whereIn("id", $data["list_id"])->delete();
        if ($del) {

            return true;
        } else {
            return false;
        }
    }

    public function getPostList($where = array())
    {
        $listData = $this->post;

        if ($where['name'])  $listData = $listData->where('name', 'like', '%' . $where['name'] . '%');

        $listData = $listData->limit(50)->get();

        return $listData;
    }

    public function getError($post_id)
    {
        $currentPost = $this->post->find($post_id)->content;
        $arrContent = explode(" ", $currentPost);
        // Get Error of Relationship
        $listError = $this->post->getPostRel()->find($post_id)->error;
        // Get Error of seperate word
        foreach ($arrContent as $word) {
            $tempErrors = $this->error->where('word', 'like', '%' . $word . '%')->get();
            $listError = $listError->merge($tempErrors);
        }
        return $listError;
    }
}
