<?php

namespace App\Repositories\Repository;

use App\Repositories\Interfaces\ErrorRepositoryInterface;
use App\Models\Error;
use App\Models\ErrorPostRel;

class ErrorRepository implements ErrorRepositoryInterface
{
    private $error;
    public function __construct()
    {
        $this->error = new Error();
        $this->errorPostRel = new ErrorPostRel();
    }

    public function get($id, $columns = array('*'))
    {
        $data = $this->error->find($id, $columns);
        if ($data) {
            return $data;
        }
        return null;
    }
    public function all($columns = array('*'))
    {
        $listData = $this->error->whereNotNull('word')->get($columns);
        return $listData;
    }
    public function paginate($perPage = 15, $columns = array('*'))
    {
        $listData = $this->error->paginate($perPage, $columns);
        return $listData;
    }
    public function save(array $data)
    {
        return $this->error->create($data);
    }
    public function saveNote(array $data)
    {
        $errorCreated = $this->error->create($data);
        $this->errorPostRel->create(
            ['error_id' => $errorCreated->id, 'post_id' => $data['post_id']]
        );
        return $errorCreated;
    }
    public function update(array $data, $id)
    {
        $dep =  $this->error->find($id);
        if ($dep) {
            foreach ($dep->getFillable() as $field) {
                if (array_key_exists($field, $data)) {
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public function getByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->error->where($column, $value)->first();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->error;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->first();


        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByColumn($column, $value, $columnsSelected = array('*'))
    {

        $data = $this->error->where($column, $value)->get();
        if ($data) {
            return $data;
        }
        return null;
    }
    public function getListByMultiColumn(array $where, $columnsSelected = array('*'))
    {

        $data = $this->error;

        foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();

        if ($data) {
            return $data;
        }
        return null;
    }
    public function delete($id)
    {
        $del = $this->error->find($id);
        if ($del !== null) {
            $del->delete();
            return true;
        } else {
            return false;
        }
    }

    public function deleteMulti(array $data)
    {
        $del = $this->error->whereIn("id", $data["list_id"])->delete();
        if ($del) {

            return true;
        } else {
            return false;
        }
    }

    public function getErrorLib($where = array())
    {
        $listData = $this->error->whereNotNull('word');

        if ($where['word'])  $listData = $listData->where('word', 'like', '%' . $where['word'] . '%');

        $listData = $listData->limit(50)->get();

        return $listData;
    }
}
