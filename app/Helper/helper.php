<?php
use Carbon\Carbon;

function getNow()
{
	return Carbon::now();
}