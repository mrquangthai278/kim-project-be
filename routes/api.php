<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Api not require Login
//User API
Route::post('auth/register', 'Api\UserController@register');
Route::post('auth/login', 'Api\UserController@login');
Route::get('/user/{email}', 'Api\UserController@getUserbyEmail');


//Api require Login
// Route::group(['middleware' => 'jwt.auth'], function () {
// });

//API for Error
Route::get('error', 'Api\ErrorController@getListError');
Route::get('error/all', 'Api\ErrorController@all');
Route::post('error', 'Api\ErrorController@create');
Route::post('error/note', 'Api\ErrorController@createNote');
Route::delete('error', 'Api\ErrorController@delete');
Route::put('error', 'Api\ErrorController@update');

//API for Post
Route::get('post', 'Api\PostController@getListPost');
Route::get('post/all', 'Api\PostController@all');
Route::post('post', 'Api\PostController@create');
Route::delete('post', 'Api\PostController@delete');
Route::put('post', 'Api\PostController@update');
Route::get('post/{id}/error', 'Api\PostController@getError');

//API for Link
Route::post('link', 'Api\LinkController@create');
Route::get('link', 'Api\LinkController@getLink');
Route::put('link', 'Api\LinkController@update');
Route::post('link', 'Api\LinkController@create');

//API for Exporting
Route::post('link-export', 'Api\LinkController@export');
