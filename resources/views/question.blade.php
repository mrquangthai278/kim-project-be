<form action="/api/question" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    Type:<br>
    <select name="type">
        <option value="image">image</option>
        <option value="video">video</option>
        <option value="none" selected>none</option>
    </select><br><br>
    Files :<br>
    <input type="file" name="upload_file"><br><br>
    Content:<br>
    <textarea rows="4" cols="50" name="content"></textarea><br><br>
    Result A :<br>
    <input type="text" name="resultA" value="Mickey"><br>
    Result B :<br>
    <input type="text" name="resultB" value="Mickey"><br>
    Result C :<br>
    <input type="text" name="resultC" value="Mickey"><br>
    Result D :<br>
    <input type="text" name="resultD" value="Mickey"><br><br>
    Result Exactly :<br>
    <select name="extract">
        <option value="a">A</option>
        <option value="b">B</option>
        <option value="c">C</option>
        <option value="d">D</option>
    </select><br><br>
    Level:<br>
    <select name="level">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
    </select><br><br>
    <br>
    <input type="submit" value="Submit">
</form>